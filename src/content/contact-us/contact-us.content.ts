export function createPageContent() {
    return {
        top: {
            mainImage: '/assets/img/platform/d_main_image.svg',
            mainTitle: 'Talk to a human',
            subText: 'Please provide details about your custom application development needs, or give us a call.'
        },
        form: {
            label: 'SNAPSHOT',
            formItems: [
                {
                    name: 'first_name',
                    type: 'text',
                    title: 'First Name',
                },
                {
                    name: 'last_name',
                    type: 'text',
                    title: 'Last Name',
                },
                {
                    name: '00N6A000009NgY3',
                    type: 'text',
                    title: 'Company Website',
                },
                {
                    name: 'email',
                    type: 'email',
                    title: 'Company Email',
                },
                {
                    name: 'phone',
                    type: 'phone',
                    title: 'Preferred Phone',
                },
                {
                    name: '00N6A00000PHCLJ',
                    type: 'text',
                    title: 'What is the primary business objective of your digital transformation initiative?',
                }
            ],
            checkBoxItems: [
                'Web',
                'Mobile',
                'IoT',
                'Blockchain',
                'ML/AI',
                'NLP',
                'GCP',
                'Other',
            ]
        },
        location: {
            label: 'TEAMGIG HQ',
            title: 'Where we\'re located',
            subText: 'Teamgig is headquartered in the tech capital of the world. Visit us in downtown San Francisco’s bustling SoMa neighborhood.',
            address: [
                '301 Howard Street',
                'Suite 2100',
                'San Francisco, CA 94105',
                '+1 (941) 888-4447',
            ],
            locationImages: [
                '/assets/img/company/location_icon_1.jpg',
                '/assets/img/company/location_icon_2.jpg',
            ],
            urls: [
                'https://www.google.com/maps/place/301+Howard+St+%232100,+San+Francisco,+CA+94105/@37.7895493,-122.3961699,17z/data=!4m5!3m4!1s0x80858064ca640e7f:0xbe146f272161bc00!8m2!3d37.7895408!4d-122.394228?hl=en'
            ]
        },
        common: {
            labels: [
                'Please provide a brief description of your project [500 characters]',
                'What is the initial approved budget?',
                'Do you require particular technical expertise?',
            ],
            amounts: [
                '$150k-$300k',
                '$300k-$500k',
                '$500k-$1MM',
                '$1MM+',
            ],
            call: [
                'Call',
                '1-833-TEAMGIG'
            ],
            button: 'Submit',
            urls: [
                'https://webto.salesforce.com/servlet/servlet.WebToLead?encoding=UTF-8'
            ]
        },
        footer: {
            image: '/assets/img/company/footer.svg',
            mainTitle: 'Let\'s build great things together.',
            buttonTitle: 'Become a Teamgig'
        },
    };
}

