export function createPageContent() {
    return {
        footerLinks: [
            {
                category: "Solutions",
                links: [
                    {
                        title: "Innovation",
                        link: "/solutions/innovation"
                    },
                    {
                        title: "Prototyping",
                        link: "/solutions/prototyping"
                    },
                    {
                        title: "Design",
                        link: "/solutions/design"
                    },
                    {
                        title: "AI",
                        link: "/solutions/artificialintelligence"
                    },
                    {
                        title: "Development",
                        link: "/solutions/development"
                    },
                    {
                        title: "Maintenance",
                        link: "/solutions/maintenance"
                    }
                ],
            },
            {
                category: "Platform",
                links: [
                    {
                        title: "Talent Network",
                        link: "/platform/talent"
                    },
                    {
                        title: "Solutions Delivery",
                        link: "/platform/difference"
                    }
                ],
            },
            {
                category: "Company",
                links: [
                    {
                        title: "About",
                        link: "/company/about"
                    },
                    {
                        title: "Careers",
                        link: "/company/careers"
                    }
                ],
            },
            {
                category: "Clients",
                links: [
                    {
                        title: "Client Stories",
                        link: "/clients/clients"
                    },
                    {
                        title: "Enterprise",
                        link: "/clients//enterprise"
                    },
                    {
                        title: "FAQ",
                        link: "/clients/faq"
                    }
                ],  
            }
        ],
    content: {
        back: "Back to top",
        copy: "©",
        year: "2019",
        inc: ", Teamgig Inc.",
        links: [
            {
                title: "Terms ",
                link: "/terms",
            },
            {
                title: "Privacy",
                link: "/privacy",
            },
        ]
    }
    }
}