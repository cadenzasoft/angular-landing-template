export function createPageContent() {
  return {
    hero: {
      mainImages: [
        '/assets/img/main/main_image_1.png',
        '/assets/img/main/main_image_2.png',
        '/assets/img/main/main_image_3.png',
      ],
      mainTitle: 'Build Better Software Faster',
      subText: {
        strong: 'Teamgig',
        common: 'delivers high-impact business outcomes at startup speed.',
      },
      textIcon_1: 'CLOSE COLLABORATION',
      textIcon_2: 'HIGH QUALITY',
      buttonTitle: 'Contact us',
      subButtonText: 'to accelerate your digital transformation',
      buttonIcon: '/assets/img/main/main_button_icon.svg',
      heroIcons: [
        {
          icon: '/assets/img/main/hero_icon_1.svg',
          title: 'Managed',
        },
        {
          icon: '/assets/img/main/hero_icon_2.svg',
          title: 'Fast',
        },
        {
          icon: '/assets/img/main/hero_icon_3.svg',
          title: 'Reliable',
        },
        {
          icon: '/assets/img/main/hero_icon_4.svg',
          title: 'Expert',
        },
        {
          icon: '/assets/img/main/hero_icon_5.svg',
          title: 'Newsworthy',
        },
      ]
    },
    slider: {
      mainTitle_1: 'The world\'s leading brands design with Teamgig',
      mainTitle_2: 'The world\'s leading brands design with Teamgig',
      brandsIcons: [
        [
          '/assets/img/solutions/brands/brands_1.svg',
          '/assets/img/solutions/brands/brands_2.svg',
          '/assets/img/solutions/brands/brands_3.svg',
          '/assets/img/solutions/brands/brands_4.svg',
          '/assets/img/solutions/brands/brands_5.svg',
        ],
        [
          '/assets/img/solutions/brands/brands_6.svg',
          '/assets/img/solutions/brands/brands_7.svg',
          '/assets/img/solutions/brands/brands_8.svg',
          '/assets/img/solutions/brands/brands_9.svg',
          '/assets/img/solutions/brands/brands_10.svg',
        ]
      ]
    },
    intelligence: {
      mainTitle: {
        strong: 'at startup speed',
        common: 'Applications that matter'
      },
      subText: {
        part_1: 'Teamgig creates value for enterprise companies by delivering',
        part_2: 'high-quality custom software on demand',
      },
      subIconItems: [
        {
          subIcon: '/assets/img/main/intelligence_icon_1.svg',
          subIconTitle: 'Expert',
          subIconSubtext: 'Get access to domain experts in new technologies',
        },
        {
          subIcon: '/assets/img/main/intelligence_icon_2.svg',
          subIconTitle: 'Managed',
          subIconSubtext: 'Work with a fully-managed development service',
        },
        {
          subIcon: '/assets/img/main/intelligence_icon_3.svg',
          subIconTitle: 'Fast',
          subIconSubtext: 'Quickly spin up a new development team',
        },
        {
          subIcon: '/assets/img/main/intelligence_icon_4.svg',
          subIconTitle: 'Reliable',
          subIconSubtext: 'Ensure consistent delivery with our data-driven platform',
        },
      ],
    },
    talents: {
      network: {
        label: 'TEAMGIG TALENT NETWORK',
        mainTitle: {
          strong: 'top global talent',
          common: 'Work with'
        },
        subText: 'Our virtual teams feature product managers, designers, and developers with deep industry experience',
        iconTitle: 'VETTED TALENT',
        icon: '/assets/img/main/network_icon_1.svg'
      },
      expertise: {
        label: 'DIGITAL TRANSFORMATION EXPERTISE',
        mainTitle: {
          strong: 'new technologies',
          common: 'Transform your business with'
        },
        subtext: 'Teamgig delivers software teams on demand to solve enterprise digital challenges',
        iconTitle: 'DEEP EXPERTISE',
        icon: '/assets/img/main/network_icon_2.svg',
        subGroups: [
          {
            title: 'Artificial Intelligence',
            icon: '/assets/img/main/network_icon_3.svg',
            skils: [
              'Machine Learning',
              'NLP',
              'Deep Learning'
            ]
          },
          {
            title: 'Web Development',
            icon: '/assets/img/main/network_icon_4.svg',
            skils: [
              'Flask',
              'React',
              'Node'
            ]
          },
          {
            title: 'Mobile Development',
            icon: '/assets/img/main/network_icon_5.svg',
            skils: [
              'iOS',
              'Android',
              'React Native'
            ]
          },
          {
            title: 'Product and Design',
            icon: '/assets/img/main/network_icon_6.svg',
            skils: [
              'Product Management',
              'UX Design',
              'UI Design'
            ]
          },
        ],
      },
      stats: [
        {
          count: 400,
          skil: 'Developers'
        },
        {
          count: 200,
          skil: 'Product Managers'
        },
        {
          count: 100,
          skil: 'Designers'
        },
      ],
      buttonTitle: 'Meet the Teamgig Talent Network',
      buttonIcon: '/assets/img/main/network_button_icon.svg'
    },
    company: {
      mainTitle: {
        strong: 'choose Teamgig',
        common: 'See why top companies'
      },
      score: [
        {
          title: 'PIB',
          subText: 'PIB worked with Teamgig to streamline the onboarding process for private investment banking.',
          scoreIcon: '/assets/img/main/company_1.jpg'
        },
        {
          title: 'CanopyTax',
          subText: 'Canopy worked with Teamgig to create a learning management system for tax professionals.',
          scoreIcon: '/assets/img/main/company_2.jpg'
        }
      ],
      buttonTitle: 'See our client stories',
      buttonIcon: '/assets/img/main/company_button_icon.svg'
    },
    technology: {
      mainTitle: {
        strong: 'powered by technology',
        common: 'Reliable, efficient delivery'
      },
      subText: 'The Teamgig Solution Delivery platform uses millions of project datapoints to ensure that your project is successful',
      cards: [
        {
          title: 'Project Health',
          subText: 'Dashboards monitor activity to identify project roadblocks',
          icon: '/assets/img/main/technology_icon_1.svg'
        },
        {
          title: 'Karma',
          subText: 'Powerful peer rating ensures team quality across the Teamgig Talent Network',
          icon: '/assets/img/main/technology_icon_2.svg'
        },
        {
          title: 'Project Planner',
          subText: 'Ideal plans by application type leverage past project experience',
          icon: '/assets/img/main/technology_icon_3.svg'
        }
      ]
    },
    highlight: {
      label: 'PRESS HIGHLIGHTS',
      mainTitle: {
        strong: 'saying about Teamgig',
        common: 'See what people are'
      },
    },
    sliderText: [
      'Teamgig’s CTO Predicts Future Belongs to Edge Computing, Deep AI and Embedded Analytics',
      'Teamgig CEO On Why Digital Transformation Requires A Startup Mentalit',
      'Podcast: Accelerating the Delivery of Digital Transformation Applications',
      'Challenges of the Modern Retail CIO & CMO',
      'Blockchain Gains Ground, But Hurdles Remain',
      '10 Questions Developers Should Ask Employers During a Job Interview',
      '10 Smart Ways for STEM Majors to Make Money on the Side',
      'Teamgig Books $10M to Pair Software Developers With Corporate Projects',
      'Teamgig Does The Dev Dirty Work To Turn Your Idea Into An App',
      'Teamgig analyzes your request, figures out the best team for the job--including programmers, product...',
      'One of its key selling points is that many of its engineers are moonlighters with day jobs at presti...',
      'The science of teams',
      'Andreessen Horowitz leads $10M round for on-demand developer startup Teamgig',
      'Teamgig Plans to Tap IBM Cloud as Preferred Platform to Enable Developers to Build Next-Generation, ...',
      'They combine Silicon Valley-based product managers with elite developers from a vetted talent pool t...'
    ],
    viewButton: 'View Article',
    contact: {
      mainTitle: 'Ready to get started?',
      buttonTitle: 'Contact us'
    },
    career: {
      careerItems: [
        {
          mainTitle: {
            strong: 'platform of the future',
            common: 'Help build the software delivery'
          },
          icon: '/assets/img/main/career_icon_1.svg',
          label: 'TEAMGIG TEAM',
          subText: 'Join a talented team changing development',
          buttonTitle: 'Join Teamgig Team'
        },
        {
          mainTitle: {
            strong: 'designer, dev, or PM?',
            common: 'Are you a talented'
          },
          icon: '/assets/img/main/career_icon_2.svg',
          label: 'TEAMGIG TALENT NETWORK',
          subText: 'Work on interesting projects with smart people',
          buttonTitle: 'Join Teamgig Team'
        },
      ]
    },
    urls: [
      'https://aithority.com/the-future/future-belongs-to-edge-computing-deep-ai-and-embedded-analytics/',
      'https://chiefexecutive.net/teamgig-ceo-digital-transformation-startup-mentality/',
      'https://techblogwriter.co.uk/teamgig/',
      'https://risnews.com/challenges-modern-retail-cio-cmo',
      'https://www.cmswire.com/information-management/blockchain-gains-ground-but-hurdles-remain/',
      'https://www.techrepublic.com/article/10-questions-developers-should-ask-employers-during-a-job-interview/',
      'https://www.entrepreneur.com/slideshow/311925#1',
      'https://blogs.wsj.com/venturecapital/2015/12/07/teamgig-books-10m-to-pair-software-developers-with-corporate-projects/',
      'https://techcrunch.com/2015/07/22/uber-for-developers/',
      'http://www.inc.com/business-insider/best-new-startups-of-2015.html',
      'http://www.forbes.com/sites/elainepofeldt/2016/07/31/can-this-startups-idea-close-the-savings-gap-for-freelancers/#4b60f7c11610',
      'http://www.forbes.com/sites/elainepofeldt/2016/07/31/can-this-startups-idea-close-the-savings-gap-for-freelancers/#4b60f7c11610',
      'http://venturebeat.com/2015/12/07/andreessen-horowitz-leads-10m-round-for-on-demand-developer-startup-teamgig/',
      'http://www-03.ibm.com/press/us/en/pressrelease/49162.wss',
      'http://mashable.com/2015/11/22/top-sites-freelance-jobs/#iAWNQ.V3Kuq7',
      'https://www.fountain.com/c/teamgig-e787c98b-90dc-41dc-a8e3-8c7d18c65e47/teamgig/US#/',
    ]
  };
}



export function createMenuLinks() {
  return [
    {
      path: 'solutions',
      title: 'Solutions'
    },
    {
      path: 'platform',
      title: 'Platform'
    },
    {
      path: 'company',
      title: 'Company'
    },
    {
      path: 'clients',
      title: 'Clients'
    },
    // {
    //   path: 'contact-us',
    //   title: 'Contact us'
    // },
    // {
    //   path: 'blog',
    //   title: 'Blog'
    // },
  ];
}

export function createSubMenuLinks() {
  return {
    solutions: [
      {
        path: '/solutions/innovation',
        title: 'Innovation'
      },
      {
        path: '/solutions/prototyping',
        title: 'Prototyping'
      },
      {
        path: '/solutions/design',
        title: 'Design'
      },
      {
        path: '/solutions/development',
        title: 'Artificial Intelligence'
      },
      {
        path: '/solutions/artificialintelligence',
        title: 'Development'
      },
      {
        path: '/solutions/maintenance',
        title: 'Maintenance'
      },
    ],
    platform: [
      {
        path: '/platform/talent',
        title: 'Teamgig Talent Network'
      },
      {
        path: '/platform/intelligence',
        title: 'Teamgig Solution Delivery'
      },
      {
        path: '/platform/difference',
        title: 'Difference'
      },
    ],
    company: [
      {
        path: '/company/about',
        title: 'About'
      },
      {
        path: '/company/careers',
        title: 'Careers'
      },
    ],
    clients: [
      {
        path: '/clients/clients',
        title: 'Clients Stories'
      },
      {
        path: '/clients/enterprise',
        title: 'Enterprise'
      },
      {
        path: '/clients/faq',
        title: 'FAQ'
      },
    ]
  };
}
