export function createPageContent() {
    return {
        tableData: {
            thead: [
                'Criteria',
                'Teamgig',
                'Employment',
                'Systems Integrators'
            ],
            tbody: [
                [
                   'Start Project',
                    '1-2 Weeks',
                    '1-4 Months',
                    '1-3 Months'
                ],
                [
                    'Recruiting Fee',
                     '$0',
                     '$10-40k',
                     '$0'
                 ],
                 [
                    'Guaranteed Quality',
                    'Very Low',
                    'Low',
                    'Very High'
                 ],
                 [
                    'Prescreening',
                 ],
                 [
                    'Termination Costs',
                    'Low',
                    'Very High',
                    'Medium',
                 ],
            ]
        }
    }
}
