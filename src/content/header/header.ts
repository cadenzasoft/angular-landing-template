export function createPageContent() {
    return {
        blockItems: {
            mainTitle: "Why Teamgig?",
            items: [
                {
                    icon: "/assets/img/top_talent_icon.svg",
                    title: "Top talent"
                },
                {
                    icon: "/assets/img/managed_icon_1.svg",
                    title: "Managed service"
                },
                {
                    icon: "/assets/img/liquid_icon_1.svg",
                    title: "Liquid workforce"
                },
                {
                    icon: "/assets/img/scalable_icon_1.svg",
                    title: "Scalable infrastructure"
                },
                {
                    icon: "/assets/img/enterprise_icon_1.svg",
                    title: "Enterprise security"
                },
            ]

        },
        solutions: {
            links: [
                {
                    title: "Innovation",
                    description: "Research cutting-edge software solutions",
                    icon: "/assets/img/nav-item1.svg",
                    link: "/solutions/innovation"
                },
                {
                    title: "Prototyping",
                    description: "Test product concepts quickly",
                    icon: "/assets/img/nav-item2.svg",
                    link: "/solutions/prototyping"
                },
                {
                    title: "Design",
                    description: "Create stunning visuals and experiences",
                    icon: "/assets/img/nav-item3.svg",
                    link: "/solutions/design"
                },
                {
                    title: "Artificial Intelligence",
                    description: "Supercharge your product with smart agents",
                    icon: "/assets/img/nav-item4.svg",
                    link: "/solutions/artificialintelligence"
                },
                {
                    title: "Development",
                    description: "Build a launch-ready product",
                    icon: "/assets/img/nav-item5.svg",
                    link: "/solutions/development"
                },
                {
                    title: "Maintenance",
                    description: "Get ongoing development support",
                    icon: "/assets/img/nav-item6.svg",
                    link: "/solutions/maintenance"
                },
            ]
        },
        platform: {
            links: [
                {
                    title: "Teamgig Talent Network",
                    description: "Access a global network of elite knowledge workers",
                    link: "/platform/talent"
                },
                {
                    title: "Teamgig Solution Delivery",
                    description: "Benefit from insights from thousands of projects",
                    link: "/platform/intelligence"
                },
                {
                    title: "Difference",
                    description: "Build applications that matter at startup speed",
                    link: "/platform/difference"
                },
            ]
        },
        company: {
            links: [
                {
                    title: "About",
                    description: "Learn how we work and what we value",
                    link: "/company/about"
                },
                {
                    title: "Careers",
                    description: "Join a team solving tough problems",
                    link: "/company/careers"
                },
            ]
        },
        clients: {
            links: [
                {
                    title: "Client Stories",
                    description: "Hear success stories from Fortune 500s and more",
                    link: "/clients/clients"
                },
                {
                    title: "Enterprise",
                    description: "See how we empower large companies",
                    link: "/clients/enterprise"
                },
                {
                    title: "FAQ",
                    description: "Find quick answers on a range of topics",
                    link: "/clients/faq"
                },
            ]
        },
        contactUs: {
            links: [
                {
                    title: "Contact Us",
                    link: "/contact-us"
                },
            ]
        }
    }
}