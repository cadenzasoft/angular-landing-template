import { Component, OnInit } from '@angular/core';
import { createPageContent } from 'src/content/header/header';
import { createPageContent as createFooterContent} from 'src/content/footer/footer';

@Component({
  selector: 'app-site-layout',
  templateUrl: './site-layout.component.html',
  styleUrls: ['./site-layout.component.sass']
})
export class SiteLayoutComponent implements OnInit {
  content: any;
  footerContent: any;
  
  constructor() { }

  ngOnInit() {
    this.content = createPageContent();
    this.footerContent = createFooterContent();
  }

}
