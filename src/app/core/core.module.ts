import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { SiteLayoutComponent } from './layouts/site-layout/site-layout.component';
import { RouterModule } from '@angular/router';
import { ErrorPageComponent } from './pages/error-page/error-page.component';
import { BodyComponent } from './components/body/body.component';
import { TermComponent } from './pages/term/term.component';
import { PrivacyComponent } from './pages/privacy/privacy.component';
import { WhySharedComponent } from './components/why/why-shared.component';

@NgModule({
  declarations: [
    HeaderComponent,
    FooterComponent,
    SiteLayoutComponent,
    ErrorPageComponent,
    BodyComponent,
    TermComponent,
    PrivacyComponent,
    WhySharedComponent
  ],
  imports: [
    CommonModule,
    RouterModule
    // RouterModule.forChild([]),
  ],
  exports: [
    BodyComponent,
    WhySharedComponent
  ],
})
export class CoreModule { }
