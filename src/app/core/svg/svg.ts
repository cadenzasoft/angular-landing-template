export function createSVG() {
    return {
        reviews: `<svg viewBox="0 0 350 200" height="200" width="350"
        data-reactid="661">
        <g data-reactid="662">
            <rect x="12" y="24" width="10" height="152" class="BarChart__background___2aQtA" data-reactid="663">
            </rect>
            <rect x="12" y="176" width="10" height="0" class="orange" data-reactid="664"></rect><text x="17"
                y="200" class="BarChart__label___7eEsq" style="font-size:16px;" data-reactid="665">1</text>
        </g>
        <g data-reactid="666">
            <rect x="46" y="24" width="10" height="152" class="BarChart__background___2aQtA" data-reactid="667">
            </rect>
            <rect x="46" y="176" width="10" height="0" class="dark" data-reactid="668"></rect><text x="51"
                y="200" class="BarChart__label___7eEsq" style="font-size:16px;" data-reactid="669">2</text>
        </g>
        <g data-reactid="670">
            <rect x="80" y="24" width="10" height="152" class="BarChart__background___2aQtA" data-reactid="671">
            </rect>
            <rect x="80" y="176" width="10" height="0" class="cyan" data-reactid="672"></rect><text x="85"
                y="200" class="BarChart__label___7eEsq" style="font-size:16px;" data-reactid="673">3</text>
        </g>
        <g data-reactid="674">
            <rect x="114" y="24" width="10" height="152" class="BarChart__background___2aQtA"
                data-reactid="675"></rect>
            <rect x="114" y="176" width="10" height="0" class="green" data-reactid="676"></rect><text x="119"
                y="200" class="BarChart__label___7eEsq" style="font-size:16px;" data-reactid="677">4</text>
        </g>
        <g data-reactid="678">
            <rect x="148" y="24" width="10" height="152" class="BarChart__background___2aQtA"
                data-reactid="679"></rect>
            <rect x="148" y="176" width="10" height="0" class="blue" data-reactid="680"></rect><text x="153"
                y="200" class="BarChart__label___7eEsq" style="font-size:16px;" data-reactid="681">5</text>
        </g>
        <g data-reactid="682">
            <rect x="182" y="24" width="10" height="152" class="BarChart__background___2aQtA"
                data-reactid="683"></rect>
            <rect x="182" y="176" width="10" height="0" class="orange" data-reactid="684"></rect><text x="187"
                y="200" class="BarChart__label___7eEsq" style="font-size:16px;" data-reactid="685">6</text>
        </g>
        <g data-reactid="686">
            <rect x="216" y="24" width="10" height="152" class="BarChart__background___2aQtA"
                data-reactid="687"></rect>
            <rect x="216" y="176" width="10" height="0" class="dark" data-reactid="688"></rect><text x="221"
                y="200" class="BarChart__label___7eEsq" style="font-size:16px;" data-reactid="689">7</text>
        </g>
        <g data-reactid="690">
            <rect x="250" y="24" width="10" height="152" class="BarChart__background___2aQtA"
                data-reactid="691"></rect>
            <rect x="250" y="127" width="10" height="49" class="cyan" data-reactid="692"></rect><text x="255"
                y="200" class="BarChart__label___7eEsq" style="font-size:16px;" data-reactid="693">8</text>
        </g>
        <g data-reactid="694">
            <rect x="284" y="24" width="10" height="152" class="BarChart__background___2aQtA"
                data-reactid="695"></rect>
            <rect x="284" y="105" width="10" height="71" class="green" data-reactid="696"></rect><text x="289"
                y="200" class="BarChart__label___7eEsq" style="font-size:16px;" data-reactid="697">9</text>
        </g>
        <g data-reactid="698">
            <rect x="318" y="24" width="10" height="152" class="BarChart__background___2aQtA"
                data-reactid="699"></rect>
            <rect x="318" y="24" width="10" height="152" class="blue" data-reactid="700"></rect><text x="323"
                y="200" class="BarChart__label___7eEsq" style="font-size:16px;" data-reactid="701">10</text>
        </g>
    </svg>`,
        top_enterprise: `<li data-reactid="316"><a class="false" data-reactid="317"><svg width="20" height="18" data-reactid="318">
    <circle cx="10" cy="9" r="6" data-reactid="319"></circle>
    <path d="M0 8.660254037844386L5 0L15 0L20 8.660254037844386L15 17.32050807568877L5 17.32050807568877Z"
            data-reactid="320"></path>
</svg><!-- react-text: 321 -->Our clients
<!-- /react-text --></a></li>
<li data-reactid="322"><a class="SideNav__active___2mzFU" data-reactid="323"><svg width="20" height="18"
    data-reactid="324">
    <circle cx="10" cy="9" r="6" data-reactid="325"></circle>
    <path d="M0 8.660254037844386L5 0L15 0L20 8.660254037844386L15 17.32050807568877L5 17.32050807568877Z"
            data-reactid="326"></path>
</svg><!-- react-text: 327 -->Why Teamgig
<!-- /react-text --></a></li>
<li data-reactid="328"><a class="false" data-reactid="329"><svg width="20" height="18" data-reactid="330">
    <circle cx="10" cy="9" r="6" data-reactid="331"></circle>
    <path d="M0 8.660254037844386L5 0L15 0L20 8.660254037844386L15 17.32050807568877L5 17.32050807568877Z"
            data-reactid="332"></path>
</svg><!-- react-text: 333 -->Best talent
<!-- /react-text --></a></li>
<li data-reactid="334"><a class="false" data-reactid="335"><svg width="20" height="18" data-reactid="336">
    <circle cx="10" cy="9" r="6" data-reactid="337"></circle>
    <path d="M0 8.660254037844386L5 0L15 0L20 8.660254037844386L15 17.32050807568877L5 17.32050807568877Z"
            data-reactid="338"></path>
</svg><!-- react-text: 339 -->Solution
<!-- /react-text --></a></li>`,
        team_chart: `<svg class="d-none d-sm-none d-md-block" data-reactid="534">
        <circle cx="50%" cy="50%" r="50" fill="none" stroke-width="10"
            stroke-dasharray="0 235 63 16" class="blue" data-reactid="535"></circle>
        <circle cx="50%" cy="50%" r="50" fill="none" stroke-width="10" stroke-dasharray="0 298 16 0"
            class="orange" data-reactid="536"></circle>
        <circle cx="50%" cy="50%" r="50" fill="none" stroke-width="10" stroke-dasharray="31 283 0"
            class="cyan" data-reactid="537"></circle>
        <circle cx="50%" cy="50%" r="50" fill="none" stroke-width="10"
            stroke-dasharray="0 31 16 267" class="dark" data-reactid="538"></circle>
        <circle cx="50%" cy="50%" r="50" fill="none" stroke-width="10"
            stroke-dasharray="0 47 100 167" class="green" data-reactid="539"></circle>
        <circle cx="50%" cy="50%" r="50" fill="none" stroke-width="10"
            stroke-dasharray="0 147 88 79" class="grey6" data-reactid="540"></circle>
        <!-- react-text: 541 -->&gt;
        <!-- /react-text -->
    </svg>`,
    about: `<li data-reactid="316"><a class="SideNav__active___2mzFU" data-reactid="317"><svg width="20" height="18"
    data-reactid="318">
    <circle cx="10" cy="9" r="6" data-reactid="319"></circle>
    <path d="M0 8.660254037844386L5 0L15 0L20 8.660254037844386L15 17.32050807568877L5 17.32050807568877Z"
            data-reactid="320"></path>
</svg><!-- react-text: 321 -->Mission
<!-- /react-text --></a></li>
<li data-reactid="322"><a class="false" data-reactid="323"><svg width="20" height="18"
    data-reactid="324">
    <circle cx="10" cy="9" r="6" data-reactid="325"></circle>
    <path d="M0 8.660254037844386L5 0L15 0L20 8.660254037844386L15 17.32050807568877L5 17.32050807568877Z"
            data-reactid="326"></path>
</svg><!-- react-text: 327 -->Snapshot
<!-- /react-text --></a></li>
<li data-reactid="328"><a class="false" data-reactid="329"><svg width="20" height="18"
    data-reactid="330">
    <circle cx="10" cy="9" r="6" data-reactid="331"></circle>
    <path d="M0 8.660254037844386L5 0L15 0L20 8.660254037844386L15 17.32050807568877L5 17.32050807568877Z"
            data-reactid="332"></path>
</svg><!-- react-text: 333 -->Press highlights
<!-- /react-text --></a></li>
<li data-reactid="334"><a class="false" data-reactid="335"><svg width="20" height="18"
    data-reactid="336">
    <circle cx="10" cy="9" r="6" data-reactid="337"></circle>
    <path d="M0 8.660254037844386L5 0L15 0L20 8.660254037844386L15 17.32050807568877L5 17.32050807568877Z"
            data-reactid="338"></path>
</svg><!-- react-text: 339 -->Teamgig Team
<!-- /react-text --></a></li>
<li data-reactid="340"><a class="false" data-reactid="341"><svg width="20" height="18"
    data-reactid="342">
    <circle cx="10" cy="9" r="6" data-reactid="343"></circle>
    <path d="M0 8.660254037844386L5 0L15 0L20 8.660254037844386L15 17.32050807568877L5 17.32050807568877Z"
            data-reactid="344"></path>
</svg><!-- react-text: 345 -->Investors
<!-- /react-text --></a></li>`,
        graph: `<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg"
        xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 253.1 99.8"
        enable-background="new 0 0 253.1 99.8" xml:space="preserve">
        <path fill="none" stroke="#FF7452" stroke-width="5" stroke-linecap="round"
            stroke-miterlimit="10" d="M2.5,67
c3-11.6,8.1-26.6,20.9-27.6C38.3,38.2,41,54.4,50.3,52.9c10.1-1.6,5.1-18.5,14.3-19.4C77.1,32.2,79.3,72.4,95,70.9
c17.4-1.6,9.8-41,28.2-43.6c17.9-2.6,22.7,32.5,34.3,34c12.4,1.6,12.5-25.1,20.6-27.5c10.6-3.1,12.5,14.3,21.2,14.3
c16.1,0,15.8-21,51.1-19.8"></path>
    </svg>`,
        network: `<svg class="d-none d-sm-none d-md-block" data-reactid="534">
        <circle cx="50%" cy="50%" r="50" fill="none" stroke-width="10"
            stroke-dasharray="0 235 63 16" class="blue" data-reactid="535"></circle>
        <circle cx="50%" cy="50%" r="50" fill="none" stroke-width="10" stroke-dasharray="0 298 16 0"
            class="orange" data-reactid="536"></circle>
        <circle cx="50%" cy="50%" r="50" fill="none" stroke-width="10" stroke-dasharray="31 283 0"
            class="cyan" data-reactid="537"></circle>
        <circle cx="50%" cy="50%" r="50" fill="none" stroke-width="10"
            stroke-dasharray="0 31 16 267" class="dark" data-reactid="538"></circle>
        <circle cx="50%" cy="50%" r="50" fill="none" stroke-width="10"
            stroke-dasharray="0 47 100 167" class="green" data-reactid="539"></circle>
        <circle cx="50%" cy="50%" r="50" fill="none" stroke-width="10"
            stroke-dasharray="0 147 88 79" class="grey6" data-reactid="540"></circle>
        <!-- react-text: 541 -->&gt;
        <!-- /react-text -->
    </svg>`
    }
}