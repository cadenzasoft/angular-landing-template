import { Component, OnInit, AfterViewInit } from '@angular/core';

@Component({
  selector: 'app-privacy',
  templateUrl: './privacy.component.html',
  styleUrls: ['./privacy.component.sass']
})
export class PrivacyComponent implements OnInit, AfterViewInit {

  constructor() { }

  ngOnInit() {

  }

  ngAfterViewInit() {
    $('.quest-menu').removeClass('sticky');
    $(window).scroll(() => {
      if ($(window).scrollTop() > 500) {
        $('.quest-menu').addClass('sticky');
      }

      if ($(window).scrollTop() < 501) {
        $('.quest-menu').removeClass('sticky');
      }

    });

    $('a').click(() => {
      $('a').removeClass('active-link');
    });

    $('[data-name]').mouseover(function() {
      const name = $(this).attr('data-name');
      if (name) {
        $('a').removeClass('active-link');
   	    $('[name=' + name + ']').addClass('active-link');
      }
   });

  }

  scrollToElement($element): void {
    $element.scrollIntoView({ behavior: 'smooth', block: 'start', inline: 'nearest' });
  }

}
