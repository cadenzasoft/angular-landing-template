import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-term',
  templateUrl: './term.component.html',
  styleUrls: ['./term.component.sass']
})
export class TermComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    $('.quest-menu').removeClass('sticky');
    $(window).scroll(() => {
      if ($(window).scrollTop() > 500) {
        $('.quest-menu').addClass('sticky');
      }

      if ($(window).scrollTop() < 501) {
        $('.quest-menu').removeClass('sticky');
      }

    });

    $('a').click(() => {
      $('a').removeClass('active-link');
    });

    $('[data-name]').mouseover(function() {
      const name = $(this).attr('data-name');
      if (name) {
        $('a').removeClass('active-link');
        $('[name=' + name + ']').addClass('active-link');
      }
    });

  }

  scrollToElement($element): void {
    $element.scrollIntoView({ behavior: 'smooth', block: 'start', inline: 'nearest' });
  }


}

