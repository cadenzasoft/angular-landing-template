import { Component, OnInit, Input } from '@angular/core';
import { createPageContent } from 'src/content/core/why.content';

@Component({
  selector: 'app-why-shared',
  templateUrl: './why-shared.component.html',
  styleUrls: ['./why-shared.component.sass']
})
export class WhySharedComponent implements OnInit {
  @Input() content!: any;
  commonContent: any
  
  constructor() {
    this.commonContent = createPageContent()
   }

  ngOnInit() {
  }

}
