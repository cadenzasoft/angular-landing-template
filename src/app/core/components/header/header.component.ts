import { Component, OnInit, AfterViewInit, OnDestroy, Input } from '@angular/core';
import { Router, NavigationStart } from '@angular/router';
import { createMenuLinks, createSubMenuLinks } from '../../../../content/main/main.content';
import * as $ from 'jquery';
import { filter } from 'rxjs/operators';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.sass']
})
export class HeaderComponent implements OnInit, AfterViewInit, OnDestroy {
  @Input() content!: object;
  allSubmenuLinks;

  constructor(private router: Router) {
    this.allSubmenuLinks = createSubMenuLinks();
  }

  showMenu = '';
  showMenuFrom = true;
  showMobile = false;
  selectedIndexLink;
  subMenuItems: any;
  selectedIndex: number;
  subscription = new Subscription();


  ngOnInit() {

    this.subscription.add(
      this.router.events.pipe(
        filter(event => event instanceof NavigationStart)
      ).subscribe(() => {
        this.showMobile = false;
      })
    );

    const links = $('.MobileNav__primaryNavItem___2B-PZ a.disabled');
    links.click(($event) => {
      $event.preventDefault();
    });

    let classFlag = false;
    $(window).scroll(() => {
      if ($(window).scrollTop() > 500 && this.router.url !== '/' && this.router.url !== '/contact-us' ) {
        $('.index__root___2ARXt').addClass('index__fixed___3IVdD');
        $('.SecondarySubnav__submenuContainer___2UVtk').addClass('SecondarySubnav__active___1oqCt');
        if ($('.index__root___2ARXt').hasClass('pos-r')) {
          $('.index__root___2ARXt').removeClass('pos-r');
          classFlag = true;
        }
      }

      if ($(window).scrollTop() < 501) {
        $('.index__root___2ARXt').removeClass('index__fixed___3IVdD');
        $('.SecondarySubnav__submenuContainer___2UVtk').removeClass('SecondarySubnav__active___1oqCt');
        if (classFlag) {
          $('.index__root___2ARXt').addClass('pos-r');
          classFlag = false;
        }
      }
    });

  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  ngAfterViewInit() {
    this.subMenuItems = $('.MobileNav__subMenu___3y_4l');
  }

  setMenuFlags(route: string, index: number) {
    this.showMenuFrom = true;
    this.showMenu = route;
    this.selectedIndexLink = index;
  }

  togleMobileMenu() {
    this.showMobile = this.showMobile ? false : true;
  }

  toggleSubmenu(index: number) {
    if (this.subMenuItems[index].classList.contains('MobileNav__open___12jtS')) {
      this.subMenuItems[index].classList.remove('MobileNav__open___12jtS');
      return;
    }
    Array.prototype.forEach.call(this.subMenuItems, (el) => {
      el.classList.remove('MobileNav__open___12jtS');
    });
    this.subMenuItems[index].classList.add('MobileNav__open___12jtS');
  }


  get links() {
    return createMenuLinks();
  }

  get submenuLinks() {
    let submenuLinks;

    if ((this.router.url.indexOf('platform') > 0)) {
      submenuLinks = this.allSubmenuLinks.platform;
    } else if ((this.router.url.indexOf('company') > 0)) {
      submenuLinks = this.allSubmenuLinks.company;
    } else if (this.router.url.indexOf('solutions') > 0) {
      submenuLinks = this.allSubmenuLinks.solutions;
    } else if (this.router.url.indexOf('clients') > 0) {
      submenuLinks = this.allSubmenuLinks.clients;
    } else {
      submenuLinks = [];
    }

    return submenuLinks;
  }

  trackBy(i: number, linkId: string) {
    return linkId;
  }

}
