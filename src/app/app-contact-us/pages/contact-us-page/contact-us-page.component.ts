import { createPageContent } from 'src/content/contact-us/contact-us.content';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-contact-us-page',
  templateUrl: './contact-us-page.component.html',
  styleUrls: ['./contact-us-page.component.sass']
})
export class ContactUsPageComponent implements OnInit {
  content;
  constructor() { }

  ngOnInit() {
    this.content = createPageContent();
  }

}
