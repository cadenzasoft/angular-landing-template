import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AppContactUsRoutingModule } from './app-contact-us-routing.module';
import { ContactUsPageComponent } from './pages/contact-us-page/contact-us-page.component';
import { TopContactComponent } from './components/top-contact/top-contact.component';
import { ContactFormComponent } from './components/contact-form/contact-form.component';
import { ContactLocationComponent } from './components/contact-location/contact-location.component';


@NgModule({
  declarations: [
    ContactUsPageComponent,
    TopContactComponent,
    ContactFormComponent,
    ContactLocationComponent
  ],
  imports: [
    CommonModule,
    AppContactUsRoutingModule
  ]
})
export class AppContactUsModule { }
