import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-contact-location',
  templateUrl: './contact-location.component.html',
  styleUrls: ['./contact-location.component.sass']
})
export class ContactLocationComponent implements OnInit {
  @Input() content;
  constructor() { }

  ngOnInit() {
  }

}
