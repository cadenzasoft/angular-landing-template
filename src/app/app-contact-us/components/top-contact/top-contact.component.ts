import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-top-contact',
  templateUrl: './top-contact.component.html',
  styleUrls: ['./top-contact.component.sass']
})
export class TopContactComponent implements OnInit {
  @Input() content;
  constructor() { }

  ngOnInit() {
  }

}
