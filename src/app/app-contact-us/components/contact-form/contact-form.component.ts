import { Component, OnInit, Input } from '@angular/core';
import { createPageContent } from 'src/content/contact-us/contact-us.content';

@Component({
  selector: 'app-contact-form',
  templateUrl: './contact-form.component.html',
  styleUrls: ['./contact-form.component.sass']
})
export class ContactFormComponent implements OnInit {
  @Input() content;
  commonContent: any
  
  constructor() { }

  ngOnInit() {
    this.commonContent = createPageContent()
  }

}
