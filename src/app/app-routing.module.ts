import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SiteLayoutComponent } from './core/layouts/site-layout/site-layout.component';
import { MainPageComponent } from './app-main/pages/main-page/main-page.component';
import { TermComponent } from './core/pages/term/term.component';
import { PrivacyComponent } from './core/pages/privacy/privacy.component';

const itemsRoutes: Routes = [
  {
    path: '',
    component: MainPageComponent,
  },
  {
    path: 'terms',
    component: TermComponent,
  },
  {
    path: 'privacy',
    component: PrivacyComponent,
  },
  {
    path: 'solutions',
    loadChildren: './app-solutions/app-solutions.module#AppSolutionsModule',
  },
  {
    path: 'platform',
    loadChildren: './app-platform/app-platform.module#AppPlatformModule',
  },
  {
    path: 'company',
    loadChildren: './app-company/app-company.module#AppCompanyModule',
  },
  {
    path: 'contact-us',
    loadChildren: './app-contact-us/app-contact-us.module#AppContactUsModule',
  },
  {
    path: 'clients',
    loadChildren: './app-clients/app-clients.module#AppClientsModule',
  }
];





// TODO -- canActivateChild: [HasCurrentUserCanActivateGuard],
const appRoutes: Routes = [
  // { path: 'authenticate', loadChildren: './authentication/authentication.module#AuthenticationModule' },
  { path: '', component: SiteLayoutComponent, children: itemsRoutes },
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
