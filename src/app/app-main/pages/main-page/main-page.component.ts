import { Component, OnInit, AfterViewInit } from '@angular/core';
import * as $ from 'jquery';
import { createPageContent } from 'src/content/main/main.content';
import { createPageContent as createWhyContent } from 'src/content/platform/difference.content';

@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.sass']
})

export class MainPageComponent implements OnInit, AfterViewInit {
  whyContent;
  content;
  count = 0;
  $jq = jQuery.noConflict();

  realTime = [0, 0, 0];
  amount = [];
  startFlag = true;

  constructor(
  ) { }

  ngAfterViewInit() {
    setInterval(() => {
      this.changeImage();
    }, 4000);
  }

  changeImage() {
    this.count++;
    if (this.count === 3) { this.count = 0; }
  }

  ngOnInit() {
    this.content = createPageContent();
    this.whyContent = createWhyContent();

    this.$jq('.faded').not('.slick-initialized').slick({
      autoplay: true,
      autoplaySpeed: 2000,
      infinite: false,
      speed: 2000,
      fade: true,
      slide: 'div',
      cssEase: 'linear',
      arrows: false
    });

    this.$jq('.multiple-items').not('.slick-initialized').slick({
      dots: true,
      infinite: false,
      speed: 300,
      slidesToShow: 3,
      slidesToScroll: 3,
      arrows: true,
      mobileFirst: true,

      responsive: [
        {
          breakpoint: 1440,
          settings: {
            dots: true,
            infinite: false,
            speed: 300,
            slidesToShow: 3,
            slidesToScroll: 3,
            arrows: true,
          }
        },
        {
          breakpoint: 1024,
          settings: {
            dots: true,
            infinite: false,
            speed: 300,
            slidesToShow: 2,
            slidesToScroll: 2,
            arrows: true,
          }
        },
        {
          breakpoint: 768,
          settings: {
            dots: true,
            infinite: false,
            speed: 300,
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: true,
          }
        },
        {
          breakpoint: 544,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
          }
        },
        {
          breakpoint: 360,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
          }
        }
      ]
    });

    this.$jq('.talent').not('.slick-initialized').slick({
      dots: true,
      infinite: false,
      speed: 300,
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: true,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            infinite: false,
            arrows: true,
            dots: true
          }
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
      ]
    });

    this.content.talents.stats.forEach(it => {
      this.amount.push(it.count);
    });
  }

  startCount() {
    if (!this.startFlag) { return; }
    if (window.pageYOffset > 2400 && window.pageYOffset < 2600) {
      this.startFlag = false;
      for (let index = 0; index < this.realTime.length; index++) {
        const int = setInterval(() => {
          this.realTime[index]++;
          if (this.realTime[index] >= this.amount[index]) {
            clearInterval(int);
          }
        }, 8);
      }
    }
  }



}
