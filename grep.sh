#!/bin/sh -ex

grepv() {
	grep -rni $1 $2 | \
	grep -v node_modules | \
	grep -v 'Binary file';
}

echo grab gigster.com
grepv gigster.com src | grep -v "Generator: Sketch " | grep -v .svg: > grep-results.txt

# echo grab .net
# grepv .com src | grep -v "Generator: Sketch " | grep -v .svg: >> grep-results.txt

